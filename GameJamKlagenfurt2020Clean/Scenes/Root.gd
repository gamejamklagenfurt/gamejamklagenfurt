extends Node

var currentScene = null;

func _ready():
	changeScene("MainMenu");

func changeScene(scene):
	if currentScene:
		remove_child(currentScene);
	currentScene = ResourceLoader.load("res://Scenes/" + scene + "/Scene.tscn").instance();
	add_child(currentScene);
