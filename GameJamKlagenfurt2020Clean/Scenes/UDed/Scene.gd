extends Control

var dir = Vector2(-100, -100);

func _process(delta):
	var container = get_node("MarginContainer/CenterContainer/HBoxContainer/VBoxContainer")
	container.rect_position += dir * delta;
	
	if container.rect_position.x > 600 || container.rect_position.x < -600:
		dir.x *= -1;
		
	if container.rect_position.y > 400 || container.rect_position.y < -400:
		dir.y *= -1;
