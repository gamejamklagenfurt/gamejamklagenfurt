extends Node2D

onready var nParent = get_parent();

func isHell():
	return nParent.isHell;

func toggleHell():
	nParent.isHell = !nParent.isHell;
	
	var suffix;
	
	if nParent.isHell:
		suffix = "hell"
	else:
		suffix = "land";
		
	get_node("/root/Root/Game/World/Landscape").tile_set = ResourceLoader.load("res://Scenes/Game/level_0_" + suffix + ".tres");
	get_node("/root/Root/Game/World/Building").tile_set = ResourceLoader.load("res://Scenes/Game/level_0_building_" + suffix + ".tres");
	get_node("/root/Root/Game/World/Roof").tile_set = ResourceLoader.load("res://Scenes/Game/level_0_roof_" + suffix + ".tres");
