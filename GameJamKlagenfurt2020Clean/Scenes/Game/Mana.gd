extends TextureProgress

onready var player = get_node("/root/Root/Game/Entities/Player")

func _process(delta):
	value = max_value * player.mana / player.manaMax;
