extends Sprite

var time = 0.0;

func _process(delta):
	time += delta;
	
	if(time > .5):
		var enemies = get_node("/root/Root/Game/Entities/Enemies");
		var enemy = enemies.get_child(0);
		
		enemies.remove_child(enemy);
		enemies.get_parent().add_child(enemy);
		
		enemy.position = position;
		
		get_parent().remove_child(self);
	
	frame = int(time * 8) % 4;
	
