extends KinematicBody2D

signal hit(source, damage);

var time = 0.0;
var direction = 2;

onready var player = get_parent().get_node("Player");

onready var nSpriteAttack = $SpriteAttack;
onready var nSpriteMove = $SpriteMove;

var attack = 0;
var speed = 40;

func _process(delta):
	time += delta;
	attack = max(0, attack - delta);
	
	if attack > 0:
		pass
	
	var move = player.position - position;
	
	if move.length() > 20:
		move_and_slide(move.normalized() * speed);
	else:
		attack = 2;
		emit_signal("hit", self, 15);
	
	if abs(move.x) > abs(move.y):
		if move.x > 0:
			direction = 1;
		else:
			direction = 3;
	else:
		if move.y > 0:
			direction = 2;
		else:
			direction = 0;
	
	if attack > 0:
		nSpriteMove.visible = false;
		nSpriteAttack.visible = true;
		nSpriteAttack.frame = direction * 2 + int(time) % 2;
	else:
		nSpriteAttack.visible = false;
		nSpriteMove.visible = true;
		nSpriteMove.frame = direction * 4 + int(time * 4) % 4;
