extends KinematicBody2D

onready var time = 0.0;

onready var direction = 2;

onready var nParent = get_parent();

onready var nPlayerHuman = $PlayerHuman;
onready var nPlayerDemon = $PlayerDemon;
onready var nPlayerCurrent = nPlayerHuman;

onready var spriteIdle = nPlayerCurrent.get_node("SpriteIdle");
onready var spriteMove = nPlayerCurrent.get_node("SpriteMove");
onready var spriteAtck = nPlayerCurrent.get_node("SpriteAtck");

#Player Stats
onready var health = 100
onready var healthMax = 100
onready var health_regen = 10
onready var mana = 50
onready var manaMax = 50
onready var mana_regen = 5
onready var speed = 64;

onready var attackTime = 0.0;
onready var attackVersion = 0;
onready var ability = 0.0;

func isHell():
	return nParent.isHell();

func _ready():
	remove_child(nPlayerDemon);

func _process(delta):
	time += delta;
	
	attackTime = max(0, attackTime - delta);
	ability = max(0, ability - delta);
	
	if health < 0:
		get_parent().get_parent().get_parent().changeScene("UDed");
	
	health = min(health + health_regen * delta, healthMax)
	mana = min(mana + mana_regen * delta, healthMax);
	
	var move = Vector2(0, 0);
	
	if Input.is_action_just_pressed("gameplay_exit"):
		get_parent().get_parent().get_parent().changeScene("MainMenu");
	
	if attackTime == 0 && ability == 0:
		if Input.is_action_just_pressed("gameplay_transform"):
			
			nParent.toggleHell();
			
			if isHell():
				add_child(nPlayerDemon);
				remove_child(nPlayerHuman);
				nPlayerCurrent = nPlayerDemon;
			else:
				add_child(nPlayerHuman);
				remove_child(nPlayerDemon);
				nPlayerCurrent = nPlayerHuman;
			
			spriteIdle = nPlayerCurrent.get_node("SpriteIdle");
			spriteMove = nPlayerCurrent.get_node("SpriteMove");
			spriteAtck = nPlayerCurrent.get_node("SpriteAtck");
		
		if Input.is_action_pressed("gameplay_right"):
			direction = 1;
			move.x += 1
		
		if Input.is_action_pressed("gameplay_left"):
			direction = 3;
			move.x -= 1;
		
		if Input.is_action_pressed("gameplay_up"):
			direction = 0;
			move.y -= 1;
		
		if Input.is_action_pressed("gameplay_down"):
			direction = 2;
			move.y += 1;
		
		if Input.is_action_pressed("gameplay_attack_right"):
			direction = 1;
			time = 0;
			attackTime = .5;
			attackVersion = randi() % 2;
		
		if Input.is_action_pressed("gameplay_attack_left"):
			direction = 3;
			time = 0;
			attackTime = .5;
			attackVersion = randi() % 2;
		
		if Input.is_action_pressed("gameplay_attack_up"):
			direction = 0;
			time = 0;
			attackTime = .5;
			attackVersion = randi() % 2;
		
		if Input.is_action_pressed("gameplay_attack_down"):
			direction = 2;
			time = 0;
			attackTime = .5;
			attackVersion = randi() % 2;
		
		if Input.is_action_just_pressed("gameplay_ability") && mana >= manaMax / 2:
			time = 0;
			ability = 1;
			mana -= manaMax / 2;
		
		if Input.is_action_pressed("gameplay_attack_right") || Input.is_action_pressed("gameplay_attack_left") || Input.is_action_pressed("gameplay_attack_up") || Input.is_action_pressed("gameplay_attack_down"):
			var slash = KinematicBody2D.new();
			slash.set_script(ResourceLoader.load("res://Scenes/Game/Attack.gd"));
			slash.position = position;
			slash.setDirection(direction);
			nParent.add_child(slash);

	var _unused = move_and_slide(move.normalized() * speed);
	
	for sprite in nPlayerCurrent.get_children():
		sprite.visible = false;
	
	if(attackTime > 0):
		spriteAtck.visible = true;
		spriteAtck.frame = direction * 8 + attackVersion * 4 + (int(time * 8) % 4)
	elif(move != Vector2(0, 0)):
		spriteMove.visible = true;
		spriteMove.frame = direction * 4 + (int(time * 4) % 4);
	else:
		spriteIdle.visible = true;
		spriteIdle.frame = int(time * 2) % 2;


func _on_Blob_hit(source, damage):
	if nPlayerCurrent == nPlayerHuman && ability > 0:
		source.get_parent().remove_child(source);
	else:
		health -= damage;
		move_and_collide((position - source.position).normalized() * speed / 4);


func _on_Wolf_hit(source, damage):
	if nPlayerCurrent == nPlayerHuman && ability > 0:
		source.get_parent().remove_child(source);
	else:
		health -= damage;
		move_and_collide((position - source.position).normalized() * speed / 4);
