extends KinematicBody2D

var time = 0.0;
var direction;
var sprite;

func setDirection(dir):
	direction = dir;
	sprite = Sprite.new();
	sprite.texture = ResourceLoader.load("res://Assets/Sprites/Entities/fx/slashes.png");
	sprite.hframes = 3;
	sprite.vframes = 4;
		
	add_child(sprite);
	
	if(direction == 0):
		position.y -= 8;
	if(direction == 1):
		position.x += 8;
	if(direction == 2):
		position.y += 8;
	if(direction == 3):
		position.x -= 8;
	
	position.y += 6;
	
	var rectShape = RectangleShape2D.new();
	rectShape.extents.x = 4;
	rectShape.extents.y = 4;
	
	var collisionShape = CollisionShape2D.new();
	collisionShape.shape = rectShape;
	
	add_child(collisionShape);

func _process(delta):
	time += delta;
	
	if time >= .5:
		get_parent().remove_child(self);
	
	var move = Vector2(0, 0);
	if(direction == 0):
		move.y = -1;
	if(direction == 1):
		move.x = 1;
	if(direction == 2):
		move.y = 1;
	if(direction == 3):
		move.x = -1;
	
	var collision = move_and_collide(move * 20 * delta);
	
	if collision && collision.collider is KinematicBody2D && collision.collider.name != "Player":
		collision.collider.get_parent().remove_child(collision.collider);
	
	sprite.frame = direction * 3 + (int(time * 6) % 3);
