extends Node2D

onready var nParent = get_parent();

func _process(_delta):
	if nParent.isHell():
		visible = false;
		return;
	visible = true;
