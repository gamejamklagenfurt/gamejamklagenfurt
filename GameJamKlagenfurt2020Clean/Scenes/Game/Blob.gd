extends KinematicBody2D

var time = 0.0;
var direction = 2;

var speed = 20;

onready var nPlayer = get_parent().get_parent().get_node("Player");
onready var nSprite = $Sprite;

signal hit(source, damage);

func _process(delta):
	time += delta;
	
	var move = (nPlayer.position - position).normalized();
	
	if abs(move.x) > abs(move.y):
		if move.x > 0:
			direction = 1;
		else:
			direction = 3;
	else:
		if move.y > 0:
			direction = 2;
		else:
			direction = 0;
	
	var collision = move_and_collide(move * speed * delta);
	
	if collision:
		if collision.collider == nPlayer:
			move_and_collide(-move * speed);
			emit_signal("hit", self, 15)
	
	nSprite.frame = direction * 4 + (int(time * 4) % 4);
	
	
