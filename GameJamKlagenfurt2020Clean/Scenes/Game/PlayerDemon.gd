extends Node2D

onready var nParent = get_parent();
onready var nSprite = $SpriteAblt;
onready var nDisable = $SpriteIdle;

func _process(delta):
	
	if !nParent.isHell():
		visible = false;
		return;
	visible = true;
	
	if nParent.ability > 0:
		nDisable.visible = false;
		nSprite.visible = true;
		nSprite.frame = nParent.direction * 16 + (int(nParent.time * 16) % 16);
		
		var dir = Vector2(0, 0);
		
		if nParent.direction == 0:
			dir.y = -1;
		
		if nParent.direction == 1:
			dir.x = 1;
		
		if nParent.direction == 2:
			dir.y = 1;
		
		if nParent.direction == 3:
			dir.x = -1;
			
		var collider = nParent.move_and_collide(dir * 2 * nParent.speed * delta);
		
		if collider:
			if collider.collider in get_node("/root/Root/Game/World/Placeableobjects").get_children():
				var node = Sprite.new()
				node.texture = ResourceLoader.load("res://Assets/Sprites/Interactibles/RockGoBreak.png");
				node.hframes = 4;
				node.position = collider.collider.position;
				node.set_script(ResourceLoader.load("res://Scenes/Game/RockGoBreak.gd"));
				
				collider.collider.get_parent().add_child(node);
				collider.collider.get_parent().remove_child(collider.collider);
			else:
				if collider.collider in get_parent().get_parent().get_children():
					get_parent().get_parent().remove_child(collider.collider);
	
